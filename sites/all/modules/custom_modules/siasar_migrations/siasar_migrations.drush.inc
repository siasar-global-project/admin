<?php

/**
 * Drush command logic.
 * drush_[COMMAND_NAME]().
 */
function drush_siasar_migrations_fix_entityform_migrations() {
  $fem_revCache = [];
  $parentMigration = drush_get_option('mig', '');
  $revisionMigration = drush_get_option('rev', $parentMigration . "Revisions");

  if ($parentMigration == '') {
    drush_print('Required set a migration with "--mig="');
    return;
  }

  drush_print('Processing field closed revision references in ' . $parentMigration . ' / ' . $revisionMigration . '...');
  Database::closeConnection('default', 'default');
  // Data query.
  $query = Database::getConnection('default', 'default')
    ->select('migrate_map_' . strtolower($parentMigration), 'sp');

  $query->addField('fcr', 'entity_type');
  $query->addField('fcr', 'bundle');
  $query->addField('fcr', 'entity_id');
  $query->addField('fcr', 'revision_id');
  $query->addField('fcr', 'delta');
  $query->addField('fcr', 'field_closed_revisions_value');

  $query->innerJoin('field_data_field_closed_revisions', 'fcr', 'fcr.entity_id = sp.destid1');
  /** @var DatabaseStatementBase $exe */
  $exe = $query->execute();

  $res = $exe->fetchAll();
  if (count($res) > 0) {
    drush_print('Processing ' . count($res) . ' items in data table...');
    $cnt = 0;
    $output = [];
    foreach ($res as $item) {
      $newId = fem_translateRevision($item->field_closed_revisions_value, $fem_revCache, $revisionMigration);
      // Update item.
      if ($newId) {
        $db = Database::getConnection('default', 'default');
        $upQuery = $db->update('field_data_field_closed_revisions');
        $upQuery->fields(['field_closed_revisions_value' => $newId]);
        $upQuery->condition('entity_type', $item->entity_type, '=');
        $upQuery->condition('bundle', $item->bundle, '=');
        $upQuery->condition('entity_id', $item->entity_id, '=');
        $upQuery->condition('delta', $item->delta, '=');
        $upQuery->execute();
      }
      // Prepare output.
      $output[] = fem_formatNewId($item->field_closed_revisions_value, $newId);
      ++$cnt;
      if ($cnt >= 10) {
        drush_print(implode(', ', $output));
        $cnt = 0;
        $output = [];
      }
    }
    if (count($output) > 0) {
      drush_print(implode(', ', $output));
    }
  }

  // Revisions query.
  $db = Database::getConnection('default', 'default');
  $query = $db->select('migrate_map_' . $parentMigration, 'sp');
  $query->innerJoin('entityform_revision', 'efr', 'sp.destid1 = efr.entityform_id');
  $query->innerJoin('field_revision_field_closed_revisions', 'fcr', 'efr.vid = fcr.revision_id');

  $query->addField('fcr', 'entity_type');
  $query->addField('fcr', 'bundle');
  $query->addField('fcr', 'entity_id');
  $query->addField('fcr', 'revision_id');
  $query->addField('fcr', 'delta');
  $query->addField('fcr', 'field_closed_revisions_value');
  /** @var DatabaseStatementBase $exe */
  $exe = $query->execute();

  $res = $exe->fetchAll();
  if (count($res) > 0) {
    drush_print('Processing ' . count($res) . ' items in revision table...');
    $cnt = 0;
    $output = [];
    foreach ($res as $item) {
      $newId = fem_translateRevision($item->field_closed_revisions_value, $fem_revCache, $revisionMigration);
      // Update item.
      if ($newId) {
        $db = Database::getConnection('default', 'default');
        $query = $db->update('field_revision_field_closed_revisions');
        $query->fields(['field_closed_revisions_value' => $newId]);
        $query->condition('entity_type', $item->entity_type, '=');
        $query->condition('bundle', $item->bundle, '=');
        $query->condition('entity_id', $item->entity_id, '=');
        $query->condition('revision_id', $item->revision_id, '=');
        $query->condition('delta', $item->delta, '=');
        $query->execute();
      }
      // Prepare output.
      $output[] = fem_formatNewId($item->field_closed_revisions_value, $newId);
      ++$cnt;
      if ($cnt >= 10) {
        drush_print(implode(', ', $output));
        $cnt = 0;
        $output = [];
      }
    }
    if (count($output) > 0) {
      drush_print(implode(', ', $output));
    }
  }

  drush_print('Reverting last revisions in ' . $parentMigration . '...');

  // Get entityform migrated list.
  $query = Database::getConnection('default', 'default')
    ->select('migrate_map_' . strtolower($parentMigration), 'sp');
  $query->addField('sp', 'destid1');
  $efIds = $query->execute()->fetchCol();

  $qRev = Database::getConnection('default', 'default')
    ->select('entityform_revision', 'efr');
  $qRev->distinct();
  //$qRev->addField('efr', 'entityform_id');
  $qRev->condition('efr.entityform_id', $efIds, 'IN');
  $qRev->orderBy('vid', 'ASC');
  //$qRev->range(0, 1);
  $revs = $qRev->execute()->fetchCol();

  // For each entityform revision, rever it.
  $total = count($revs);
  $cnt = 0;
  $pcnt = 0;
  $output = [];
  $processed = [];
  foreach ($revs as $fid) {
    ++$pcnt;
    $revision = entity_revision_load('entityform', $fid);
    if (isset($processed[$revision->entityform_id])) {
      // Don't process the some entityform.
      continue;
    }
    $revision->revision = 1;
    $revision->is_default = 1;
    $revision->log = "Set last revision from migration.";
    $revision->save();
    $processed[$revision->entityform_id] = TRUE;
    // Prepare output.
    $output[] = $revision->entityform_id;
    ++$cnt;
    if ($cnt >= 10) {
      $percent = round(($pcnt * 100) / $total, 0);
      drush_print('Processed [ ' . $percent . ' % ]: ' . implode(', ', $output));
      $cnt = 0;
      $output = [];
    }
  }
  if (count($output) > 0) {
    drush_print('Processed [ 100 % ]: ' . implode(', ', $output));
  }

  drush_print('Process end.');
}

/**
 * Format the new revision ID.
 *
 * @param string $oldId
 *   The old ID, used to get string lenght.
 * @param string $newId
 *   The detected new ID.
 *
 * @return string
 *   The new ID or spaces.
 */
function fem_formatNewId($oldId, $newId) {
  if (!$newId) {
    $newId = str_repeat('.', strlen($oldId));
  }
  return $oldId . '=' . $newId;
}

/**
 * Translate revision using local cache.
 *
 * @param string $src
 *   Source revision.
 * @param array $fem_revCache
 *   Revision translation cache.
 * @param array $revisionMigration
 *   Revision migration machine name.
 */
function fem_translateRevision($src, &$fem_revCache, $revisionMigration) {
  if (!isset($fem_revCache[$src])) {
    // Find revision to update.
    // SELECT * FROM migrate_map_serviceproviderrevision where sourceid1 = 229745
    $query = Database::getConnection('default', 'default')
      ->select('migrate_map_' . strtolower($revisionMigration), 'spr');
    $query->addField('spr', 'destid2');
    $query->condition('spr.sourceid1', $src, '=');
    /** @var DatabaseStatementBase $exe */
    $exe = $query->execute();
    $res = $exe->fetchCol(0);
    $fem_revCache[$src] = current($res);
  }
  if (!isset($fem_revCache[$src])) {
    // Missed revision.
    drupal_set_message('Revision ' . $src . ' not found.' , 'warning');
    $fem_revCache[$src] = FALSE;
  }
  return $fem_revCache[$src];
}
