<?php

/**
 * Migration class for users.
 */
class UsersMigration extends Migration {

  /**
   * {@inheritDoc}
   */
  public function __construct($arguments = []) {
    parent::__construct($arguments);

    $query = Database::getConnection('default', 'legacy')->select('users', 'u');
    $query->join('field_data_field_pais', 'country', "country.entity_id = u.uid AND country.entity_type = 'user'");
    $query->leftJoin('field_data_field_entidad_local', 'div', "div.entity_id = u.uid AND div.entity_type = 'user'");
    $query->leftJoin('field_data_field_institucion', 'inst', "inst.entity_id = u.uid AND inst.entity_type = 'user'");
    $query->leftJoin('field_data_field_nombre_completo', 'name', "name.entity_id = u.uid AND name.entity_type = 'user'");
    $query->leftJoin('field_data_field_identificacion', 'ident', "ident.entity_id = u.uid AND ident.entity_type = 'user'");
    $query->leftJoin('field_data_field_telefono', 'tel', "tel.entity_id = u.uid AND tel.entity_type = 'user'");
    $query->fields('u');
    $query->fields('country', ['field_pais_iso2']);
    $query->fields('div', ['field_entidad_local_tid']);
    $query->fields('inst', ['field_institucion_tid']);
    $query->fields('name', ['field_nombre_completo_value']);
    $query->fields('ident', ['field_identificacion_value']);
    $query->fields('tel', ['field_telefono_value']);
    $query->condition('country.field_pais_iso2', 'KG');

    $this->source = new MigrateSourceSQL($query, ['roles' => 'Roles ID'], NULL, ['map_joinable' => FALSE]);
    $this->destination = new MigrateDestinationUser();
    $this->map = new MigrateSQLMap($this->machineName, [
      'uid' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'description' => 'ID of destination user',
      ],
    ],
    MigrateDestinationUser::getKeySchema());

    $this->addFieldMapping('name', 'name');
    $this->addFieldMapping('pass', 'pass');
    $this->addFieldMapping('mail', 'mail');
    $this->addFieldMapping('roles', 'roles');
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('access', 'access');
    $this->addFieldMapping('login', 'login');
    $this->addFieldMapping('status', 'status');
    $this->addFieldMapping('timezone', 'timezone');
    $this->addFieldMapping('language', 'language');
    $this->addFieldMapping('init', 'init');
    $this->addFieldMapping('data', 'data');
    $this->addFieldMapping('field_pais', 'field_pais_iso2');
    $this->addFieldMapping('field_entidad_local', 'field_entidad_local_tid')
      ->sourceMigration('AdministrativeDivision');
    $this->addFieldMapping('field_entidad_local:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_institucion', 'field_institucion_tid');
    $this->addFieldMapping('field_institucion:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_nombre_completo', 'field_nombre_completo_value');
    $this->addFieldMapping('field_identificacion', 'field_identificacion_value');
    $this->addFieldMapping('field_telefono', 'field_telefono_value');
  }

  /**
   * {@inheritDoc}
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    $row_source_key = $this->source->getCurrentKey();

    // Check if the current user ID exists, and ignore if the country is KG or
    // has the same name.
    if (!empty($row_source_key) && isset($row_source_key['uid'])) {
      $user = user_load($row_source_key['uid']);

      if ($user && ($user->name === $row->name || $user->field_pais[LANGUAGE_NONE][0]['iso2'] === "KG")) {
        return FALSE;
      }
    }

    // Set roles.
    $query = Database::getConnection('default', 'legacy')->select('users_roles', 'u');
    $query->addField('u', 'rid');
    $query->condition('uid', $row_source_key);

    $results = $query->execute()->fetchCol();
    $row->roles = $results;

    // Deserialize data field.
    if (!empty($row->data)) {
      $row->data = unserialize($row->data);
    }

    return TRUE;
  }

}
