<?php

/**
 * entityform migrate.
 */
class MigrateDestinationEntityform extends MigrateDestinationEntity {

  protected $bypassDestIdCheck = FALSE;

  protected $destinationFields = [];

  /**
   * Basic initialization
   *
   * @param string $bundle
   *  A.k.a. the content type (page, article, etc.) of the node.
   * @param array $options
   *  Options applied to nodes.
   */
  public function __construct($bundle, array $options = array()) {
    parent::__construct('entityform', $bundle, $options);

    $this->info = entity_get_info('entityform');
    $this->id = isset($this->info['entity keys']['name']) ? $this->info['entity keys']['name'] : $this->info['entity keys']['id'];
    $this->revision = isset($this->info['entity keys']['revision']) ? $this->info['entity keys']['revision'] : NULL;

    $this->destinationFields = [
      'entityform_id' => t('Entityform: <a href="@doc">Existing entityform ID</a>', array('@doc' => 'http://drupal.org/node/1349696#nid')),
    ];
  }

  static public function getKeySchema() {
    return [
      'entityform_id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('The source ID'),
      ],
    ];
  }

  /**
   * Return an options array (language, text_format), used for creating fields.
   *
   * @param string $language
   * @param string $text_format
   */
  static public function options($language, $text_format) {
    return compact('language', 'text_format');
  }

  /**
   * Add extra destination fields.
   *
   * @param array $destinationFields
   *   Keys: machine names of the fields (to be passed to addFieldMapping)
   *   Values: Human-friendly descriptions of the fields.
   */
  public function addDestinationFields($destinationFields) {
    foreach ($destinationFields as $fieldName => $description) {
      $this->destinationFields[$fieldName] = $description;
    }
  }

  /**
   * Returns a list of fields available to be mapped for entities attached to
   * a particular bundle.
   *
   * @param Migration $migration
   *  Optionally, the migration containing this destination.
   * @return array
   *  Keys: machine names of the fields (to be passed to addFieldMapping)
   *  Values: Human-friendly descriptions of the fields.
   */
  public function fields($migration = NULL) {
    $properties = entity_get_property_info($this->entityType);
    $fields = $this->destinationFields;

    foreach ($properties['properties'] as $name => $property_info) {
      if (isset($property_info['setter callback'])) {
        $fields[$name] = $property_info['description'];
      }
    }

    // Then add in anything provided by handlers
    $fields += migrate_handler_invoke_all('Entity', 'fields', $this->entityType, $this->bundle);

    return $fields;
  }

  /**
   * Deletes multiple entities.
   *
   * @param array $ids
   *   An array of entity ids of the entities to delete.
   */
  public function bulkRollback(array $ids) {
    migrate_instrument_start('entityform_delete_multiple');
    $this->prepareRollback($ids);
    $result = entity_delete_multiple($this->entityType, $ids);
    $this->completeRollback($ids);
    migrate_instrument_stop('entityform_delete_multiple');

    return $result;
  }

  /**
   * Imports a single entity.
   *
   * @param stdClass $entity
   *   Generic entity object, refilled with any fields mapped in the Migration.
   * @param stdClass $row
   *   Raw source data object - passed through to prepare/complete handlers.
   *
   * @return array
   *   An array of key fields (entity id, and revision id if applicable) of the
   *   entity that was saved if successful. FALSE on failure.
   */
  public function import(stdClass $entity, stdClass $row) {
    $migration = Migration::currentMigration();

    // Updating previously-migrated content?
    if (isset($row->migrate_map_destid1) && !$this->bypassDestIdCheck) {
      if (isset($entity->{$this->id})) {
        if ($entity->{$this->id} != $row->migrate_map_destid1) {
          throw new MigrateException(t("Incoming id !id and map destination id !destid1 don't match",
            array('!id' => $entity->{$this->id}, '!destid1' => $row->migrate_map_destid1)));
        }
      }
      else {
        $entity->{$this->id} = $row->migrate_map_destid1;
      }
    }
    elseif ($migration->getSystemOfRecord() == Migration::SOURCE) {
      unset($entity->{$this->id});
    }

    if (isset($row->migrate_map_destid2)) {
      if (isset($entity->{$this->revision})) {
        if ($entity->{$this->revision} != $row->migrate_map_destid2) {
          throw new MigrateException(t("Incoming revision !id and map destination revision !destid2 don't match",
            array('!id' => $entity->{$this->revision}, '!destid2' => $row->migrate_map_destid2)));
        }
      }
      else {
        $entity->{$this->revision} = $row->migrate_map_destid2;
      }
    }

    if ($migration->getSystemOfRecord() == Migration::DESTINATION) {
      if (!isset($entity->{$this->id})) {
        throw new MigrateException(t('System-of-record is DESTINATION, but no destination id provided'));
      }
      // Load the entity that's being updated, update its values, then
      // substitute the (fake) passed in entity with that one.
      $old_entity = entity_load_single($this->entityType, $entity->{$this->id});
      if (empty($old_entity)) {
        throw new MigrateException(t("Failed to load entity of type %type and id %id", array('%type' => $this->entityType, '%id' => $entity->{$this->id})));
      }

      // Prepare the entity to get the right array structure.
      $this->prepare($entity, $row);

      foreach ($entity as $field => $value) {
        $old_entity->$field = $entity->$field;
      }
      $entity = $old_entity;
    }
    else {
      // New revision.
      if (isset($entity->revision) && $entity->revision) {
        $old_entity = entity_load_single($this->entityType, $row->migrate_map_destid1);
        if (empty($old_entity)) {
          throw new MigrateException(
            t("Failed to load entity of type %type and id %id, rev. %vid", 
            [
              '%type' => $this->entityType,
              '%id' => $entity->{$this->id},
              '%vid' => $entity->{$this->revision},
            ])
          );
        }

        $new_entity = $old_entity;
        $new_entity->revision = TRUE;
        $new_entity->default_revision = TRUE;
        $new_entity->is_new_revision = TRUE;

        foreach ($entity as $field => $value) {
          $new_entity->$field = $entity->$field;
        }
        $entity = $new_entity;
      }
      // New entity.
      else {
        // Create a real entity object, update its values with the ones we have
        // and pass it along.
        $new_entity = array();
        if (!empty($this->bundle) && !empty($this->info['entity keys']['bundle'])) {
          $new_entity[$this->info['entity keys']['bundle']] = $this->bundle;
        }
        $new_entity = entity_create($this->entityType, $new_entity);
        foreach ($entity as $field => $value) {
          $new_entity->$field = $entity->$field;
        }

        // If a destination id exists, the entity is obviously not new.
        if (!empty($new_entity->{$this->id}) && isset($new_entity->is_new)) {
          unset($new_entity->is_new);
        }
        $entity = $new_entity;
      }
      $this->prepare($entity, $row);
    }

    $updating = (!empty($entity->{$this->id}) && empty($entity->is_new));

    migrate_instrument_start('entity_save');
    $entity->is_migrating = TRUE;
    entity_save($this->entityType, $entity);
    // It's probably not worth keeping the static cache around.
    entity_get_controller($this->entityType)->resetCache();
    migrate_instrument_stop('entity_save');

    $migration->complete($entity, $row);

    if (isset($entity->{$this->id}) && $entity->{$this->id} > 0) {
      if ($updating) {
        $this->numUpdated++;
      }
      else {
        $this->numCreated++;
      }
      $return = array($entity->{$this->id});

      if (isset($entity->{$this->revision}) && $entity->{$this->revision} > 0) {
        $return[] = $entity->{$this->revision};
      }
      return $return;
    }
    return FALSE;
  }

  /**
   * Clear the field cache after an import or rollback.
   */
  public function postImport() {
    field_cache_clear();
  }
  public function postRollback() {
    field_cache_clear();
  }
  
}

/**
 * Allows you to import revisions.
 *
 * Adapted from
 * http://www.darrenmothersele.com/blog/2012/07/16/migrating-node-revisions-drupal-7/
 *
 * Class MigrateDestinationNodeRevision
 *
 * @author darrenmothersele
 * @author cthos
 */
class MigrateDestinationEntityformRevision extends MigrateDestinationEntityform {

  /**
   * Basic initialization.
   *
   * @see parent::__construct
   *
   * @param string $bundle
   *   A.k.a. the content type (page, article, etc.) of the node.
   * @param array $options
   *   Options applied to nodes.
   */
  public function __construct($bundle, array $options = array()) {
    parent::__construct($bundle, $options);

    $this->bypassDestIdCheck = TRUE;
  }

  /**
   * Get key schema for the node revision destination.
   *
   * @see MigrateDestination::getKeySchema
   *
   * @return array
   *   Returns the key schema.
   */
  static public function getKeySchema() {
    return [
      'vid' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('The current entityform revision'),
      ],
    ];
  }

  /**
   * Returns additional fields on top of node destinations.
   *
   * @param string $migration
   *   Active migration
   *
   * @return array
   *   Fields.
   */
  public function fields($migration = NULL) {
    $fields = parent::fields($migration);
    $fields['vid'] = t('Entityform: <a href="@doc">Revision (vid)</a>', array('@doc' => 'http://drupal.org/node/1298724'));
    return $fields;
  }

  /**
   * Rolls back any versions that have been created.
   *
   * @param array $entityform_ids
   *   Entityform ids to roll back.
   */
  public function bulkRollback(array $entityform_ids) {
    // Get real VIDs.
    $migration = Migration::currentMigration();
    $vids = db_select('migrate_map_' . strtolower($migration->getMachineName()), 'mr')
      ->fields('mr', ['destid2'])
      ->condition('destid1', $entityform_ids, 'IN')
      ->execute()
      ->fetchCol();

    // Revisions rollback.
    migrate_instrument_start('revision_delete_multiple');
    $this->prepareRollback($vids);
    $eids = array();
    foreach ($vids as $vid) {
      if ($revision = $this->entityform_load(NULL, $vid)) {
        db_delete('entityform_revision')
          ->condition('vid', $revision->vid)
          ->execute();
        module_invoke_all('entityform_revision_delete', $revision);
        field_attach_delete_revision('entityform', $revision);
        $eids[$revision->entityform_id] = $revision->entityform_id;
      }
    }
    $this->completeRollback($vids);
    foreach ($eids as $eid) {
      $vid = db_select('entityform_revision', 'er')
        ->fields('er', array('vid'))
        ->condition('entityform_id', $eid, '=')
        ->execute()
        ->fetchField();
      if (!empty($vid)) {
        db_update('entityform')
          ->fields(array('vid' => $vid))
          ->condition('entityform_id', $eid, '=')
          ->execute();
      }
    }
    migrate_instrument_stop('revision_delete_multiple');
  }

  /**
   * Loads a entityform object from the database.
   *
   * @param $eid
   *   The entityform ID.
   * @param $vid
   *   The revision ID.
   * @param $reset
   *   Whether to reset the entity_load cache.
   *
   * @return
   *   A fully-populated entityform object, or FALSE if the entityform is not found.
   */
  protected function entityform_load($eid = NULL, $vid = NULL, $reset = FALSE) {
    $eids = (isset($eid) ? array($eid) : array());
    $conditions = (isset($vid) ? array('vid' => $vid) : array());
    $entityform = entity_load('entityform', $eids, $conditions, $reset);
    return $entityform ? reset($entityform) : FALSE;
  }

  /**
   * Overridden import method.
   *
   * This is done because parent::import will return the entityform_id of the newly
   * created entityforms. This is bad since the migrate_map_* table will have
   * entityform_ids instead of vids, which could cause a nightmare explosion on
   * rollback.
   *
   * @param stdClass $entityform
   *   Populated entity.
   *
   * @param stdClass $row
   *   Source information in object format.
   *
   * @return array|bool
   *   Array with newly created vid, or FALSE on error.
   *
   * @throws MigrateException
   */
  public function import(stdClass $entityform, stdClass $row) {
    // We're importing revisions, this should be set.
    $entityform->revision = 1;

    if (empty($entityform->{$this->id})) {
      throw new MigrateException(t("Missing incoming {$this->id}."));
    }

    $original_updated = $this->numUpdated;

    $resp = parent::import($entityform, $row);

    // Reset num updated and increment created since new revision is always an update.
    $this->numUpdated = $original_updated;
    $this->numCreated++;

    return $resp;
  }

}
