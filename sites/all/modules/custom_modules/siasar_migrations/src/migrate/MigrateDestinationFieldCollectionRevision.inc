<?php

/**
 * @file
 * Support for the Migrate API.
 *
 * Your field collection migration should be run after the host entity
 * migration. For example, if the collection is attached to nodes via a field
 * named 'field_attached_data', and if the nodes are being imported by
 * ArticleMigration, your collection migration class constructor should look
 * like:
 *
 * @code
 *   $this->dependencies = array('Article');
 *
 *   $this->destination = new MigrateDestinationFieldCollection(
 *     'field_attached_data',
 *     array('host_entity_type' => 'node')
 *   );
 *
 *   $this->addFieldMapping('host_entity_id', 'source_article_id')
 *     ->sourceMigration('Article');
 * @endcode
 *
 * @see http://drupal.org/node/1900640
 */

/**
 * Destination class implementing migration into field_collection.
 */
class MigrateDestinationFieldCollectionRevision extends MigrateDestinationEntity {
  /**
   * The type of entity hosting this collection field (e.g., node).
   *
   * @var string
   */
  protected $hostEntityType;

  protected $destinationFields = [];

  /**
   * Database connection.
   *
   * @var DatabaseConnection
   */
  protected $connection;

  static public function getKeySchema() {
    return array(
      'revision_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'ID of field collection item revision',
      ),
    );
  }

  /**
   * Basic initialization.
   *
   * @param string $bundle
   *   Bundle name.
   * @param array $options
   *   (optional) Options applied to collections.
   */
  public function __construct($bundle, array $options = array()) {
    parent::__construct('field_collection_item', $bundle, $options);
    $this->hostEntityType = $options['host_entity_type'];
    $this->connection = Database::getConnection('default', 'default');
  }

  /**
   * Add extra destination fields.
   *
   * @param array $destinationFields
   *   Keys: machine names of the fields (to be passed to addFieldMapping)
   *   Values: Human-friendly descriptions of the fields.
   */
  public function addDestinationFields($destinationFields) {
    foreach ($destinationFields as $fieldName => $description) {
      $this->destinationFields[$fieldName] = $description;
    }
  }

  /**
   * Returns a list of fields available to be mapped for this collection
   * (bundle).
   *
   * @return array
   *   Keys: machine names of the fields (to be passed to addFieldMapping).
   *   Values: Human-friendly descriptions of the fields.
   */
  public function fields() {
    $fields = migrate_handler_invoke_all('Entity', 'fields', $this->entityType, $this->bundle);
    foreach ($this->destinationFields as $fieldName => $description) {
      $fields[$fieldName] = $description;
    }
    return $fields;
  }

  /**
   * Import a single field collection item.
   *
   * @param $collection
   *   Collection object to build. Pre-filled with any fields mapped in the
   *   migration.
   * @param $row
   *   Raw source data object - passed through to prepare/complete handlers.
   *
   * @return array|false
   *   Array of key fields (item_id only in this case) of the collection that
   *   was saved or FALSE on failure.
   */
  public function import(stdClass $collection, stdClass $row) {
    /*
     * $row content:
     * class stdClass#585 (17) {
     *
     *   ------- Field collection item revision IDs.
     *   revision_id                    => string(6) "870834"
     *   fcir_item_id                   => string(6) "531432"
     *
     *   ------- Entity form.
     *   entityform_type                => string(21) "prestador_de_servicio"
     *   entityform_id                  => string(6) "175061"
     *   entityformt_vid                => string(6) "148587"
     *
     *   ------- Field collection item.
     *   fci_field_name                 => string(14) "field_tecnicos"
     *   fci_archived                   => string(1) "0"
     *
     *   ------- Field revision data.
     *   frft_language                  => string(3) "und"
     *   ...
     *
     *   ------- Migrate trash.
     *   migrate_map_hash               => string(0) ""
     * }
     */
//var_dump($collection, $row);
    $updating = FALSE;
    if (isset($row->migrate_map_destid1)) {
      // We're updated an existing entity - start from the previous data.
      // entity_load() returns an array, so we get the field collection entity
      // with array_shift().
      throw new MigrateException('Update Field Collection Revisions not implemented.');
//      if ($entity = array_shift(entity_load('field_collection_item', array($row->migrate_map_destid1), array(), TRUE))) {
//        $entity_old = clone $entity;
//        $updating = TRUE;
//      }
    }

    if (!$updating) {
      // Skip the collection if it has no host.
      if (empty($collection->entityform_id)) {
        throw new MigrateException('Could not find host entity of the field collection to import.');
      }

      // Clean caches.
      entity_get_controller('entityform')->resetCache();
      entity_get_controller('field_collection_item')->resetCache();

      $entity = current(entity_load('field_collection_item', [$collection->item_id], [], TRUE));
      $entity_old = clone $entity;

      $host_entity = entity_revision_load('entityform', $collection->vid['destid2']);
      $host_entity->is_migrating = TRUE;

      // Prepare the entity to get the right array structure.
//      var_dump($row);
//      try {
//          $this->prepare($entity, $row);
//      }
//      catch (Exception $exc) {
//        echo $exc->getMessage();
//        echo $exc->getTraceAsString();
//      }
//      var_dump($row);

      // Field name with field wrapper.
      $migration = Migration::currentMigration();
      $wrappers = $migration->getFieldWrappersSettings();
      $fieldsWithWraper = [];
      // Load field data.
      foreach ($entity as $field => $value) {
        if ('field_' != substr($field, 0, 6)) {
          continue;
        }
        elseif (property_exists($entity_old, $field) && !property_exists($collection, $field)) {
          $entity->$field = $entity_old->$field;
        }
        else {
          $entity->$field = $collection->$field;
        }
        $fieldsWithWraper[] = $field;
        // Fix date fields.
        if (isset($wrappers[$field])) {
          $wrapper = $wrappers[$field]['instance'];
          if ($wrapper && $wrapper->getFieldType() == 'date' && is_string($entity->$field)) {
            $entity->$field = [
              $row->frft_language => [
                0 => [
                  'value' => $entity->$field,
                ],
              ],
            ];
          }
        }
      }
      $entity->default_revision = FALSE;
      $entity->revision = TRUE;
      $entity->is_new_revision = TRUE;
      try {
        $entity->updateHostEntity($host_entity, 'entityform');
      }
      catch (Exception $exc) {
        //echo "FAIL updateHostEntity: " . $exc->getMessage();
        //echo $exc->getTraceAsString();
      }

      $updating = FALSE;
    }

    // Save FC revision.
    migrate_instrument_start('field_collection_save');
    $entity->is_migrating = TRUE;
    $entity->save(TRUE);
    // Create field content.
    $tableName = 'field_revision_' . $row->fci_field_name;
    // Count other tuplas with some entityform revision to get delta.
    $query = $this->connection->select($tableName, 'fr');
    $query->addField('fr', 'delta');
    $query->condition('revision_id', $collection->vid['destid2']);
    $query->orderBy('delta', 'DESC');
    $query->range(0, 1);
    $lastDelta = $query->execute()->fetchField();
    // Insert the new entityform field tupla.
    $query = $this->connection->insert($tableName);
    $query->fields([
      'entity_type' => 'entityform',
      'bundle' => $row->entityform_type,
      'deleted' => 0,
      'entity_id' => $collection->vid['destid1'],
      'revision_id' => $collection->vid['destid2'],
      'language' => $collection->language,
      'delta' => ($lastDelta === FALSE) ? 0 : $lastDelta + 1,
      $row->fci_field_name . '_value' => $entity->item_id,
      $row->fci_field_name . '_revision_id' => $entity->revision_id,
    ]);
    $query->execute();
    // Insert each field collection field data.
    foreach ($fieldsWithWraper as $fieldName) {
      if (isset($wrappers[$fieldName])) {
        $wrapper = $wrappers[$fieldName]['instance'];
        if ($wrapper) {
          $wrapper->saveFieldData($this->connection, $entity, $collection->language, $collection->$fieldName, $row->fci_field_name);
        }
      }
    }
    migrate_instrument_stop('field_collection_save');
//var_dump($collection->vid);
    $this->complete($entity, $row);
    if ($updating) {
      $this->numUpdated++;
    }
    else {
      $this->numCreated++;
    }
    // Return the new FC revision ID.
    return array($entity->revision_id);
  }

  /**
   * Delete a migrated collection.
   *
   * @param $key
   *   Array of fields representing the key.
   */
  public function rollback(array $key) {
//    $item_id = reset($key);
//
//    $this->prepareRollback($item_id);
//    $field_collection_item = field_collection_item_load($item_id);
//    // If the collection wasn't imported then we can't roll it back, so check if
//    // the loaded object is an instance of the FieldCollectionItemEntity class.
//    if ($field_collection_item instanceof FieldCollectionItemEntity) {
//      $field_collection_item->delete();
//    }
//
//    $this->completeRollback($item_id);
//    return TRUE;
  }
}
