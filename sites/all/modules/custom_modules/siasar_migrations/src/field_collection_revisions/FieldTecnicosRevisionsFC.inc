<?php

/**
 * Migration for PAT entityforms.
 */
class FieldTecnicosRevisionsFC extends SiasarFieldCollectionRevisionsMigrationBase {
  public function __construct($arguments = []) {
    // Copy entityform settings.
    $patArg = $arguments;
    $patArg['machine_name'] = 'FieldTecnicos';
    $this->setCountry('KG');
    $migParent = new FieldTecnicosFC($patArg);

    $this->setFieldCollectionName($migParent->getBundleId());
    $this->addFieldWrappers($migParent->getFieldWrappers());

    // Prepare migration.
    $this->description = "Migration of '{$this->entityFormMachineName}' Entityform submissions revisions from SIASAR database";
    $this->dependencies = ['FieldTecnicos'];
    // Set source migration to get host entity.
    $this->sourceMigration = [
      'fc_source' => 'FieldTecnicos',
      'ef_source' => 'ServiceProvider',
      'ef_rev_source' => 'ServiceProviderRevisions',
    ];
    parent::__construct($arguments);
  }

  /**
   * Child class conditions.
   */
  protected function customConditions() {
    // Custom conditions.
//    $this->query->condition(FieldBaseWrapper::getDefaultTableAlias('field_pais') . '.' . 'field_pais_iso2', 'KG');
  }

  /**
   * Child class conditions.
   */
  protected function customCountConditions() {
    $countryField = new FieldCountryWrapper('field_pais');
    $countryField->setMigration($this);
    $countryField->setBaseTableName('field_revision_');
    $countryField->addTables($this->count_query);
    // Custom conditions.
    $this->count_query->condition(FieldBaseWrapper::getDefaultTableAlias('field_pais') . '.' . 'field_pais_iso2', 'KG');
  }

}
