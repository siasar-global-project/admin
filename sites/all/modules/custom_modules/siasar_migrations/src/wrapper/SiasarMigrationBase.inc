<?php

/**
 * Migration base for Siasar entityforms.
 */
class SiasarMigrationBase extends Migration implements SiasarMigrationInterface {

  /**
   * Query.
   *
   * @var SelectQueryInterface
   */
  protected $query;
  protected $count_query;

  /**
   * Field wrappers FieldBaseWrapper array.
   *
   * @var array
   */
  protected $fieldWrappers = array();

  /**
   * The entity form type machine name.
   *
   * @var string
   */
  protected $entityFormMachineName;

  public function __construct($arguments = []) {
    parent::__construct($arguments);

    // Subquery used to select the first revision when importing entityforms.
    $this->subqueryFirstRevision = Database::getConnection('default', 'legacy')->select('entityform', 'eform');
    $this->subqueryFirstRevision->join('entityform_revision', 'r', 'eform.entityform_id = r.entityform_id');
    $this->subqueryFirstRevision->condition('eform.type', $this->entityFormMachineName, '=');
    $this->subqueryFirstRevision->groupBy('r.entityform_id');
    $this->subqueryFirstRevision->addExpression('MIN(r.vid)', 'vid');

    // Query to get all entityforms.
    $db = Database::getConnection('default', 'legacy');
    $this->query = $db->select('entityform', 'eform');
    $this->query->distinct();

    // Join with field tables.
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addTables($this->query);
    }
    $this->query->join('entityform_revision', 'r', 'eform.entityform_id = r.entityform_id');
    $this->query->join($this->subqueryFirstRevision, 'minrev', 'r.vid = minrev.vid');

    // Fields values.
    $this->query->fields('eform');
    // Add wrapper fields.
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addFields($this->query);
    }

    // Conditions.
    $this->query->condition('eform.type', $this->entityFormMachineName);

    // Add wrappers conditions.
    $this->customConditions();

    // Sub fields.
    $extraSourceFields = [
      'created' => 'Creation date',
      'uid' => 'Author user id',
    ];
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addSourceSubFields($extraSourceFields);
    }

    // Count query to optimize user interfaces.
    $this->count_query = Database::getConnection('default', 'legacy')->select('entityform', 'eform');
    $this->count_query->join('entityform_revision', 'r', 'eform.entityform_id = r.entityform_id');
    $this->count_query->join($this->subqueryFirstRevision, 'minrev', 'r.vid = minrev.vid');
    $this->count_query->condition('eform.type', $this->entityFormMachineName, '=');
    $this->count_query->addExpression('COUNT(r.entityform_id)', 'cnt');

    $this->customCountConditions();
    // Dev count query.
    //var_dump($this->entityFormMachineName);
    //var_dump($this->count_query->__toString());

    // Set migration source.
    $this->source = new MigrateSourceSQL($this->query, $extraSourceFields, $this->count_query, ['map_joinable' => FALSE]);
    // Set migration destination.
    $this->destination = new MigrateDestinationEntityform($this->entityFormMachineName);
    $extraDestinationFields = [
      'uid' => 'Author user id',
    ];
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addDestinationSubFields($extraDestinationFields);
    }
    $this->destination->addDestinationFields($extraDestinationFields);

    // Set rows map.
    $this->map = new MigrateSQLMap($this->machineName, [
      'entityform_id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('The source ID'),
      ],
    ],
    MigrateDestinationEntityAPI::getKeySchema('entityform'));

    // Add fields mapping.
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('uid', 'uid');
    // Add wrapper field map.
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addFieldMapping($this);
    }
    // Unmigrated fields.
    $unmigratedDestinations = [
      'draft',
      'path',
    ];
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addUnmigratedDestinationSubFields($unmigratedDestinations);
    }
    $this->addUnmigratedDestinations($unmigratedDestinations);
  }

  /**
   * Get field wrappers info.
   *
   * @return array
   *   Get field wrappers array indexed by field name and value is a array
   *   with if it's multivalued, and field type.
   * @throws \Exception
   */
  public function getFieldWrappersSettings() {
    $resp = [];
    /** @var FieldBaseWrapper $wrapper */
    foreach ($this->fieldWrappers as $wrapper) {
      $item = [
        'type' => $wrapper->getFieldType(),
        'multivalue' => $wrapper->isMultivalue(),
        'name' => $wrapper->getFieldName(),
      ];
      $resp[$wrapper->getFieldName()] = $item;
    }

    return $resp;
  }

  /**
   * Child class conditions.
   */
  protected function customConditions() {}

  /**
   * Child class conditions.
   */
  protected function customCountConditions() {}

  /**
   * Set entity form type ID.
   *
   * @param string $id
   *   The entity form ID.
   */
  protected function setBundleId($id) {
    $this->entityFormMachineName = $id;
  }

  /**
   * The entity form ID.
   *
   * @return string
   *   The entity form ID.
   */
  public function getBundleId() {
    return $this->entityFormMachineName;
  }

  /**
   * Add a field wrapper.
   *
   * @param FieldBaseWrapper $wrapper
   */
  public function addFieldWrapper(&$wrapper) {
    $wrapper->setMigration($this);
    $this->fieldWrappers[] = $wrapper;
  }

  /**
   * Return all field wrappers.
   *
   * @return array
   */
  public function getFieldWrappers() {
    return $this->fieldWrappers;
  }

  /**
   * Add multiple field wrappers.
   *
   * @param array $wrappers
   */
  public function addFieldWrappers($wrappers) {
    // Update migration instance.
    /** @var FieldBaseWrapper $fieldWrapper */
    foreach ($wrappers as $fieldWrapper) {
      $this->addFieldWrapper($fieldWrapper);
    }
  }

  /**
   * Return the join condition to add fields properly.
   *
   * @param string $fieldName
   * @param string $tableAlias
   * @param string $wrapperClass
   * @param \SelectQueryInterface $query
   *
   * @return string
   */
  public function getFieldJoinCondition($fieldName, $tableAlias, $wrapperClass, $query) {
    return "{$tableAlias}.entity_id = eform.entityform_id AND {$tableAlias}.entity_type = 'entityform' AND {$tableAlias}.language = 'und'";
  }

  /**
   * Complete migration function.
   *
   * @param $entityForm
   * @param $row
   */
  public function complete($entityForm, $row) {}

  /**
   * {@inheritDoc}
   */
  public function prepareRow($row) {
    // Process fields that have more than one value.
    foreach ($this->getFieldWrappers() as $wrapper) {
      // Check if the field is declared as multi-value one.
      if ($wrapper->isMultivalue()) {
        // Get all values for this field.
        $query = Database::getConnection('default', 'legacy')->select($wrapper->getBaseTableName(), 'field');
        $query->addField('field', ($wrapper->databaseValueColumn) ? $wrapper->databaseValueColumn : $wrapper->getFieldName() . '_value');
        $query->condition('entity_type', 'entityform');
        $query->condition('entity_id', $row->entityform_id);

        $results = $query->execute()->fetchCol();

        // If not empty, set results in the current row.
        if (!empty($results)) {
          $field_mapping_id = ($wrapper->databaseValueColumn) ? $wrapper->databaseValueColumn : $wrapper->getFieldName() . '_value';
          $row->$field_mapping_id = $results;
        }
      }
    }

    // Process UID field.
    if (!empty($row->uid)) {
      $query = db_select('migrate_map_users', 'map');
      $query->fields('map', ['destid1']);
      $query->condition('sourceid1', $row->uid);
      $query->isNotNull('destid1');

      $results = $query->execute()->fetchCol();

      if (!empty($results)) {
        // Assign the new uid.
        $row->uid = current($results);
      }
    }

    // Process field user reference values.
    if (!empty($row->field_user_reference_target_id)) {
      $query = db_select('migrate_map_users', 'map');
      $query->fields('map', ['destid1']);
      $query->condition('sourceid1', $row->field_user_reference_target_id);
      $query->isNotNull('destid1');

      $results = $query->execute()->fetchCol();

      if (!empty($results)) {
        // Assign the new uid.
        $row->field_user_reference_target_id = current($results);
      }
    }

    parent::prepareRow($row);
  }

}
