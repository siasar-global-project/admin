<?php

/**
 * Base migration class for field collection items.
 */
class SiasarFieldCollectionRevisionsMigrationBase extends Migration implements SiasarMigrationInterface {

  /**
   * Query.
   *
   * @var SelectQueryInterface
   */
  protected $query;
  
  /**
   * Count query.
   *
   * @var SelectQueryInterface
   */
  protected $count_query;

  /**
   * Field wrappers FieldBaseWrapper array.
   *
   * @var array
   */
  protected $fieldWrappers = array();
  protected $fieldWrappersSettings = [];

  /**
   * The field collection machine name.
   *
   * @var string
   */
  protected $fcName;

  /**
   * Country to apply conditions.
   *
   * @var string
   */
  protected $country = '%';

  /**
   * Source migration name.
   *
   * @var string
   */
  protected $sourceMigration;

  public function __construct($arguments = []) {
    parent::__construct($arguments);
    // Set migration dependencies.
    $this->setDependencies();
    // Query to get all entityforms.
    $countryTableAlias = FieldBaseWrapper::getDefaultTableAlias('field_pais');
    $db = Database::getConnection('default', 'legacy');

    $this->query = $db->select('field_collection_item_revision', 'fcir');
    $this->query->join('field_collection_item', 'fci', "fci.item_id = fcir.item_id AND fci.archived = 0");
    $this->query->join('field_revision_' . $this->fcName, 'frft', "fcir.item_id = frft.{$this->fcName}_value AND fcir.revision_id = frft.{$this->fcName}_revision_id");
    $this->query->join('field_data_field_pais', 'fdfp', "fdfp.entity_id = frft.entity_id");

    // Join with field tables.
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addTables($this->query);
    }

    // Fields values.
    $this->query->addField('fcir', 'revision_id', 'revision_id');
    $this->query->addField('fcir', 'item_id', 'fcir_item_id');
    $this->query->addField('fci', 'field_name', 'fci_field_name');
    $this->query->addField('fci', 'archived', 'fci_archived');
    $this->query->addField('frft', 'language', 'frft_language');
    $this->query->addField('fdfp', 'bundle', 'entityform_type');
    $this->query->addField('frft', 'entity_id', 'entityform_id');
    $this->query->addField('frft', 'revision_id', 'entityform_vid');
    // Add wrapper fields.
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addFields($this->query);
    }

    // Conditions.
    $this->query->condition('fci.field_name', $this->fcName);
    $this->query->condition('fdfp.field_pais_iso2', $this->country);

    // !!!! QUTIAR ESTO !!!!!
//    $this->query->condition('frft.entity_id', 175932);

    // Oldest revision first.
    $this->query->orderBy('fcir.revision_id');

    // Add wrappers conditions.
    $this->customConditions();
    // Set migration source.
    $extraSourceFields = [];
    $this->source = new MigrateSourceSQL($this->query, $extraSourceFields, NULL, ['map_joinable' => FALSE]);
    // Set migration destination.
    $this->destination = new MigrateDestinationFieldCollectionRevision($this->fcName, ['host_entity_type' => 'entityform']);
    $extraDestinationFields = [];
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addDestinationSubFields($extraDestinationFields);
    }
    $this->destination->addDestinationFields($extraDestinationFields);

    // Set rows map.
    $this->map = new MigrateSQLMap($this->machineName, [
      'revision_id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('The source ID'),
      ],
    ],
    MigrateDestinationFieldCollectionRevision::getKeySchema());

    // Add fields mapping.
    $this->addFieldMapping('revision_id', 'revision_id');
    $this->addFieldMapping('item_id', 'fcir_item_id')->sourceMigration($this->sourceMigration['fc_source']);
    $this->addFieldMapping('language', 'frft_language');

    $this->addFieldMapping('entityform_id', 'entityform_id')->sourceMigration($this->sourceMigration['ef_source']);
    $this->addFieldMapping('vid', 'entityform_vid')->sourceMigration($this->sourceMigration['ef_rev_source']);

    // Add wrapper field map.
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addFieldMapping($this);
    }
  }

  /**
   * Get field wrappers info.
   *
   * @return array
   *   Get field wrappers array indexed by field name and value is a array
   *   with if it's multivalued, and field type.
   */
  public function getFieldWrappersSettings() {
    if (count($this->fieldWrappersSettings) > 0) {
      return $this->fieldWrappersSettings;
    }
    $this->fieldWrappersSettings = [];
    /** @var FieldBaseWrapper $wrapper */
    foreach ($this->fieldWrappers as $wrapper) {
      $item = [
        'type' => $wrapper->getFieldType(),
        'multivalue' => $wrapper->isMultivalue(),
        'name' => $wrapper->getFieldName(),
        'instance' => $wrapper,
      ];
      $this->fieldWrappersSettings[$wrapper->getFieldName()] = $item;
    }

    return $this->fieldWrappersSettings;
  }

  /**
   * Default implementation of prepareRow(). This method is called from the
   * source plugin upon first pulling the raw data from the source.
   *
   * @param $row
   *  Object containing raw source data.
   *
   * @return bool
   *  TRUE to process this row, FALSE to have the source skip it.
   */
  public function prepareRow($row) {
//    echo 'prepareRow($row)' . "\n";
//    var_dump($row);
    return parent::prepareRow($row);
  }

  /**
   * Child class conditions.
   */
  protected function customConditions() {}

  /**
   * Child class conditions.
   */
  protected function customCountConditions() {}

  /**
   * Set field collection name.
   *
   * @param string $name
   *   Field collection name.
   */
  protected function setFieldCollectionName($name) {
    $this->fcName = $name;
  }

  /**
   * Set country iso2 value.
   *
   * @param string $country_iso2
   */
  protected function setCountry($country_iso2) {
    $this->country = $country_iso2;
  }

  /**
   * Add a field wrapper.
   *
   * @param FieldBaseWrapper $wrapper
   */
  public function addFieldWrapper(&$wrapper) {
    $wrapper->setMigration($this);
    $wrapper->setBaseTableName('field_revision_');
    $this->fieldWrappers[] = $wrapper;
  }

  /**
   * Set dependencies for this migration.
   */
  protected function setDependencies() {}

  /**
   * {@inheritDoc}
   */
  public function getFieldJoinCondition($fieldName, $tableAlias, $wrapperClass, $query) {
    return $tableAlias . ".revision_id = fcir.revision_id AND " . $tableAlias . ".entity_type = 'field_collection_item'";
  }

  /**
   * {@inheritDoc}
   */
  public function getBundleId() {
    return $this->fcName;
  }

  /**
   * {@inheritDoc}
   */
  public function addFieldWrappers($wrappers) {
    // Update migration instance.
    /** @var FieldBaseWrapper $fieldWrapper */
    foreach ($wrappers as $fieldWrapper) {
      $this->addFieldWrapper($fieldWrapper);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getFieldWrappers() {
    return $this->fieldWrappers;
  }

}
