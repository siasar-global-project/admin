<?php

/**
 * SIASAR migrations interface.
 */
interface SiasarMigrationInterface {
  /**
   * The entity form ID.
   *
   * @return string
   *   The entity form ID.
   */
  public function getBundleId();

  /**
   * Add a field wrapper.
   *
   * @param FieldBaseWrapper $wrapper
   */
  public function addFieldWrapper(&$wrapper);

  /**
   * 
   * @param type $wrappers
   */
  public function addFieldWrappers($wrappers);

  /**
   * Get defined field wrappers.
   */
  public function getFieldWrappers();

  /**
   * Get field wrappers info.
   *
   * @return array
   *   Get field wrappers array indexed by field name and value is a array
   *   with if it's multivalued, and field type.
   */
  public function getFieldWrappersSettings();

  /**
   * Build the field JOIN condition.
   *
   * @param string $fieldName
   *   The field name.
   * @param string $tableAlias
   *   Table alias.
   * @param string $wrapperClass
   *   The field wrapper class.
   * @param SelectQueryInterface $query
   *   Query to add the join.
   *
   * @return string
   *   The JOIN condition string.
   */
  public function getFieldJoinCondition($fieldName, $tableAlias, $wrapperClass, $query);

}
