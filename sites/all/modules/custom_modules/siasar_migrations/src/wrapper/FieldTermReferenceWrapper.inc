<?php

/**
 * Wrapper for import term reference fields on migrate.
 */
class FieldTermReferenceWrapper extends FieldBaseWrapper {

  /**
   * {@inheritDoc}
   */
  public function __construct($fieldName, $multivalue = FALSE) {
    parent::__construct($fieldName, $multivalue);
    $this->source_type = 'tid';
    $this->field_type = 'termreference';
  }

  /**
   * Prepare fields.
   *
   * @param SelectQueryInterface $query
   */
  public function addFields($query) {
    if (!$this->isMultivalue()) {
      $query->fields($this->tableAlias, [$this->fieldName . '_tid']);
    }
  }

  /**
   * Prepare mappings.
   */
  public function addFieldMapping() {
    if (empty($this->source_migration)) {
      $this->migration->addFieldMapping($this->fieldName, $this->fieldName . '_tid');
    }
    else {
      $this->migration->addFieldMapping($this->fieldName, $this->fieldName . '_tid')
        ->sourceMigration($this->source_migration);
    }

    if ($this->source_type) {
      $this->migration->addFieldMapping($this->fieldName . ':source_type')->defaultValue($this->source_type);
    }
  }

  /**
   * {@inheritDoc}
   */
  protected function buildDatabaseValueColumn() {
    $this->databaseValueColumn = $this->fieldName . '_tid';
  }

  /**
   * Save field data in her table.
   *
   * @param DatabaseConnection $connection
   *   Database connecto.
   * @param object $entity
   *   Parent entity.
   * @param string $language
   *   Language code.
   * @param array $sourceData
   *   Source field data.
   * @param string $bundle
   *   Host gield name.
   *
   * @throws Exception
   */
  public function saveFieldData($connection, $entity, $language, $sourceData, $bundle) {
    $query = $connection->insert($this->getBaseTableName());
    if (!$sourceData[0]) {
      $sourceData[0] = 0;
      //watchdog(WATCHDOG_WARNING, "{$bundle} / {$this->fieldName}_tid = NULL in " . get_class($this));
    }
    $query->fields([
      'entity_type' => $entity->entityType(),
      'bundle' => $bundle,
      'deleted' => 0,
      'entity_id' => $entity->item_id,
      'revision_id' => $entity->revision_id,
      'language' => $language,
      'delta' => 0,
      $this->fieldName . '_tid' => $sourceData[0],
    ]);
    $query->execute();
  }

}
