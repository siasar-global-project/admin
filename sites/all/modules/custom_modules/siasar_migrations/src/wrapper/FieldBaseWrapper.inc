<?php

/**
 * Field to import base class.
 */
class FieldBaseWrapper {

  /**
   * Wrapped field type.
   *
   * @var string
   */
  protected $field_type = '';

  /**
   * @var string
   */
  protected $source_type = '';

  /**
   * Source migration for field mapping.
   *
   * @var string
   */
  protected $source_migration;

  /**
   * Migration field owner.
   *
   * @var SiasarMigrationInterface
   */
  protected $migration;

  /**
   * The field machine name.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * The field table alias.
   *
   * @var string
   */
  protected $tableAlias;

  /**
   * Base table name to build names in addTables().
   *
   * @var string
   */
  protected $baseTableName;

  /**
   * Is mulvivalue field?
   *
   * @var bool
   */
  protected $multivalue = FALSE;

  /**
   * @var string
   */
  public $databaseValueColumn = '';

  /**
   * Construct field wrapper.
   *
   * @param string $fieldName
   *   Field name.
   */
  public function __construct($fieldName, $multivalue = FALSE) {
    $this->fieldName = $fieldName;
    $this->baseTableName = 'field_data_';
    $this->tableAlias = self::getDefaultTableAlias($fieldName);
    $this->multivalue = $multivalue;

    $this->buildDatabaseValueColumn();
  }

  /**
   * Is multivalue?
   *
   * @return bool
   *   TRUE if is a multivalue field.
   */
  public function isMultivalue() {
    return $this->multivalue;
  }

  /**
   * Get field type.
   *
   * @return string
   *   This wrapper field type.
   *
   * @throws Exception
   */
  public function getFieldType() {
    if (empty($this->field_type)) {
      throw new Exception('Wrapper don\'t define its field type: ' . get_class($this));
    }
    return $this->field_type;
  }

  /**
   * Get field name.
   *
   * @return string
   *   Field name.
   */
  public function getFieldName() {
    return $this->fieldName;
  }

  /**
   * Set submigration settings.
   *
   * @param string $source_type
   *   Source field.
   * @param object $source_migration
   *   Source migration.
   */
  public function setSubmigration($source_type = '', $source_migration = NULL) {
    $this->source_migration = $source_migration;
    $this->source_type = $source_type;
  }

  /**
   * Update migration instance.
   *
   * @param SiasarMigrationInterface $migration
   */
  public function setMigration($migration) {
    $this->migration = $migration;
  }

  public function setBaseTableName($name) {
    $this->baseTableName = $name;
  }

  /**
   * Generate a table alias with the field name.
   *
   * @param string $fieldName
   *   Field name.
   *
   * @return string
   *   Table alias.
   */
  public static function getDefaultTableAlias($fieldName) {
    return preg_replace("/[^a-z0-9]+/", "", strtolower($fieldName));
  }

  /**
   * Prepare tables.
   *
   * @param SelectQueryInterface $query
   */
  public function addTables($query) {
    if (!$this->multivalue) {
      $query->leftJoin(
        $this->baseTableName . $this->fieldName,
        $this->tableAlias,
        $this->getJoinCondition($query)
      );
    }
  }

  /**
   * Build the JOIN condition.
   *
   * @param SelectQueryInterface $query
   *
   * @return string
   *   The JOIN condition.
   */
  protected function getJoinCondition($query) {
    return $this->migration->getFieldJoinCondition($this->fieldName, $this->tableAlias, get_class($this), $query);
  }

  /**
   * Prepare fields.
   *
   * @param SelectQueryInterface $query
   */
  public function addFields($query) {}

  /**
   * Prepare fields.
   *
   * @param array $subfields
   *   Key is the field name, value is the field description.
   */
  public function addSourceSubFields(&$subfields) {}

  /**
   * Prepare fields.
   *
   * @param array $subfields
   *   Key is the field name, value is the field description.
   */
  public function addDestinationSubFields(&$subfields) {}

  /**
   * Prepare fields.
   *
   * @param array $subfields
   *   Key is the field name, value is the field description.
   */
  public function addUnmigratedDestinationSubFields(&$subfields) {}

  /**
   * Prepare fields.
   *
   * @param array $unmigratedSource
   *   Key is the field name, value is the field description.
   */
  public function addUnmigratedSourceSubFields(&$unmigratedSource) {}

  /**
   * Prepare mappings.
   */
  public function addFieldMapping() {}

  /**
   * Function for build the field database column value name.
   */
  protected function buildDatabaseValueColumn() {}

  /**
   * Return the field base table name.
   *
   * @return string
   */
  public function getBaseTableName() {
    return $this->baseTableName . $this->fieldName;
  }

  /**
   * Save field data in her table.
   *
   * @param DatabaseConnection $connection
   *   Database connecto.
   * @param object $entity
   *   Parent entity.
   * @param string $language
   *   Language code.
   * @param array $sourceData
   *   Source field data.
   * @param string $bundle
   *   Host gield name.
   *
   * @throws Exception
   */
  public function saveFieldData($connection, $entity, $language, $sourceData, $bundle) {
    throw new Exception("Field wrapper do not implement saveFieldData method: {$this->fieldName} == " . get_class($this));
  }

}
