<?php

/**
 * Wrapper for import measure fields on migrate.
 *
 * This field has no compatibility with multi-value process.
 */
class FieldMeasureWrapper extends FieldBaseWrapper {

  /**
   * {@inheritDoc}
   */
  public function __construct($fieldName) {
    parent::__construct($fieldName);

    $this->field_type = 'measure';
  }

  /**
   * {@inheritDoc}
   */
  public function addFields($query) {
    $query->fields($this->tableAlias, ["{$this->fieldName}_value", "{$this->fieldName}_target_id"]);
  }

  /**
   * {@inheritDoc}
   */
  public function addFieldMapping() {
    $this->migration->addFieldMapping($this->fieldName, "{$this->fieldName}_value");
    $this->migration->addFieldMapping("{$this->fieldName}:target_id", "{$this->fieldName}_target_id");
  }

  /**
   * Save field data in her table.
   *
   * @param DatabaseConnection $connection
   *   Database connecto.
   * @param object $entity
   *   Parent entity.
   * @param string $language
   *   Language code.
   * @param array $sourceData
   *   Source field data.
   * @param string $bundle
   *   Host gield name.
   *
   * @throws Exception
   */
  public function saveFieldData($connection, $entity, $language, $sourceData, $bundle) {
    $query = $connection->insert($this->getBaseTableName());
    if (!$sourceData['arguments']['target_id']) {
      $sourceData['arguments']['target_id'] = 0;
      //watchdog(WATCHDOG_WARNING, "{$bundle} / {$this->fieldName}_target_id = NULL in " . get_class($this));
    }
    $query->fields([
      'entity_type' => $entity->entityType(),
      'bundle' => $bundle,
      'deleted' => 0,
      'entity_id' => $entity->item_id,
      'revision_id' => $entity->revision_id,
      'language' => $language,
      'delta' => 0,
      $this->fieldName . '_value' => $sourceData[0],
      $this->fieldName . '_target_id' => $sourceData['arguments']['target_id'],
    ]);
    $query->execute();
  }

}
