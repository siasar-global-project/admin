<?php

/**
 * Migration base for Siasar entityforms.
 *
 * @see http://www.darrenmothersele.com/blog/2012/07/16/migrating-node-revisions-drupal-7/
 */
class SiasarRevisionsMigrationBase extends Migration implements SiasarMigrationInterface {
  /**
   * Query.
   *
   * @var SelectQueryInterface
   */
  protected $query;

  /**
   * Count query.
   *
   * @var SelectQueryInterface
   */
  protected $count_query;

  /**
   * Field wrappers FieldBaseWrapper array.
   *
   * @var array
   */
  protected $fieldWrappers = array();

  /**
   * Entityform source migration machine name.
   *
   * @var type
   */
  protected $sourceMigration = '';
  /**
   * The entity form type machine name.
   *
   * @var string
   */
  protected $entityFormMachineName;

  /**
   * Current revision migration.
   *
   * Required in postImport to update field_closed_revision.
   *
   * @var SiasarMigrationBase
   */
  protected $parentMigration;

  /**
   * Revision translate cache to field closed revisions.
   *
   * @var array
   */
  private $revCache = [];

  public function __construct($arguments = []) {
    if ($this->sourceMigration == '') {
      throw new MigrateException(t("Missing migration 'sourceMigration' class attribute."));
    }

    parent::__construct($arguments);

    // Query to get all entityforms.
    $db = Database::getConnection('default', 'legacy');
    $this->query = $db->select('entityform', 'eform');
    $this->query->distinct();

    // Join with field tables.
    $this->query->join('entityform_revision', 'r', 'eform.entityform_id = r.entityform_id');
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addTables($this->query);
    }

    // Fields values.
    $this->query->fields('eform', ['entityform_id', 'type', 'language', 'created', 'data', 'uid', 'draft']);
    $this->query->addField('r', 'changed', 'changed');
    $this->query->addField('r', 'vid', 'vid');
    // Add wrapper fields.
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addFields($this->query);
    }
    $this->addFieldMapping('entityform_id', 'entityform_id')->sourceMigration($this->sourceMigration);
    $this->addFieldMapping('changed', 'changed');

    // Conditions.
    $this->query->condition('eform.type', $this->entityFormMachineName);

    // Add wrappers conditions.
    $this->customConditions();

    // Set migration source.
    $extraSourceFields = [
      'created' => 'Creation date',
      'uid' => 'Author user id',
    ];
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addSourceSubFields($extraSourceFields);
    }

    // Old revisions first.
    $this->query->orderBy('r.changed', 'ASC');

    // Count query to optimize user interfaces.
    $this->count_query = Database::getConnection('default', 'legacy')->select('entityform', 'eform');
    $this->count_query->join('entityform_revision', 'r', 'eform.entityform_id = r.entityform_id');
    $this->count_query->fields('r', ['vid']);
    $this->count_query->condition('eform.type', $this->entityFormMachineName, '=');
    //$this->count_query->addExpression('COUNT(r.entityform_id)', 'cnt');
    $this->customCountConditions();
    $this->count_query = $this->count_query->countQuery();

    // Dev count query.
    //var_dump($this->entityFormMachineName . ' => ' . $this->count_query->__toString());
    //var_dump($this->count_query->execute()->fetchCol());

    $this->source = new MigrateSourceSQL($this->query, $extraSourceFields, $this->count_query, ['map_joinable' => FALSE]);
    // Set migration destination.
    $this->destination = new MigrateDestinationEntityformRevision($this->entityFormMachineName);
    $extraDestinationFields = [
      'changed' => 'Revision change date',
      'uid' => 'Author user id',
    ];
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addDestinationSubFields($extraDestinationFields);
    }
    $this->destination->addDestinationFields($extraDestinationFields);

    // Set rows map.
    $this->map = new MigrateSQLMap($this->machineName, [
      'vid' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => t('The current entityform revision'),
      ],
    ],
    MigrateDestinationEntityAPI::getKeySchema('entityform'));

    // Add fields mapping.
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('uid', 'uid');
    // Add wrapper field map.
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addFieldMapping($this);
    }
    // Unmigrated fields.
    // Destinations.
    $unmigratedDestinations = [
      'draft',
      'path',
    ];
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addUnmigratedDestinationSubFields($unmigratedDestinations);
    }
    $this->addUnmigratedDestinations($unmigratedDestinations);
    // Sources.
    $unmigratedSource = [
      'type',
      'language',
      'data',
      'draft',
    ];
    foreach ($this->fieldWrappers as $wrapper) {
      $wrapper->addUnmigratedSourceSubFields($unmigratedSource);
    }
    $this->addUnmigratedSources($unmigratedSource);
  }

  /**
   * Get field wrappers info.
   *
   * @return array
   *   Get field wrappers array indexed by field name and value is a array
   *   with if it's multivalued, and field type.
   */
  public function getFieldWrappersSettings() {
    $resp = [];
    /** @var FieldBaseWrapper $wrapper */
    foreach ($this->fieldWrappers as $wrapper) {
      $item = [
        'type' => $wrapper->getFieldType(),
        'multivalue' => $wrapper->isMultivalue(),
        'name' => $wrapper->getFieldName(),
      ];
      $resp[$wrapper->getFieldName()] = $item;
    }

    return $resp;
  }

  /**
   *
   * @param type $row
   *
   * @see https://www.drupal.org/node/1117454#comment-6940004
   */
  function prepareRow($row) {
    parent::prepareRow($row);
    // Process fields that have more than one value.
    foreach ($this->getFieldWrappers() as $wrapper) {
      // Check if the field is declared as multi-value one.
      if ($wrapper->isMultivalue()) {
        // Get all values for this field.
        $query = Database::getConnection('default', 'legacy')->select($wrapper->getBaseTableName(), 'field');
        $query->addField('field', ($wrapper->databaseValueColumn) ? $wrapper->databaseValueColumn : $wrapper->getFieldName() . '_value');
        $query->condition('entity_type', 'entityform');
        $query->condition('entity_id', $row->entityform_id);
        $query->condition('revision_id', $row->vid);

        $exe = $query->execute();
        $results = $exe->fetchCol();

        // If not empty, set results in the current row.
        if (!empty($results)) {
          $field_mapping_id = ($wrapper->databaseValueColumn) ? $wrapper->databaseValueColumn : $wrapper->getFieldName() . '_value';
          $row->$field_mapping_id = $results;
        }
      }
    }

    // Process UID field.
    if (!empty($row->uid)) {
      $query = db_select('migrate_map_users', 'map');
      $query->fields('map', ['destid1']);
      $query->condition('sourceid1', $row->uid);
      $query->isNotNull('destid1');

      $results = $query->execute()->fetchCol();

      if (!empty($results)) {
        // Assign the new uid.
        $row->uid = current($results);
      }
    }

    // Process field user reference values.
    if (!empty($row->field_user_reference_target_id)) {
      $query = db_select('migrate_map_users', 'map');
      $query->fields('map', ['destid1']);
      $query->condition('sourceid1', $row->field_user_reference_target_id);
      $query->isNotNull('destid1');

      $results = $query->execute()->fetchCol();

      if (!empty($results)) {
        // Assign the new uid.
        $row->field_user_reference = current($results);
      }
    }

    if (!isset($row->migrate_map_destid1)) {
      /* The mappings, importantly destid1, from ContactImport table are added
       * to the row to ensure correct running of the dedupe() function.
       * The dedupe() function will not operate properly if
       * migrate_map_destid1 is not set, and this can be the case for the first
       * time the update is running.
       */
      $query = db_select('migrate_map_' . strtolower($this->machineName), 'map', array('fetch' => PDO::FETCH_ORI_FIRST));
      $query->addField('map', 'destid1');
      $query->addField('map', 'destid2');
      $query->condition('sourceid1', $row->entityform_id);
      $res = $query->execute()->fetchAssoc();
      if ($res !== FALSE) {
        // Manually push in the value entity ID.
        $row->migrate_map_destid1 = $res['destid1'];
        // Manually push in the value entity Vid.
        $row->migrate_map_destid2 = $res['destid2'];
      }
      else {
        // Find destid1 from source migration.
        $query = db_select('migrate_map_' . strtolower($this->sourceMigration), 'map', array('fetch' => PDO::FETCH_ORI_FIRST));
        $query->addField('map', 'destid1');
        $query->condition('sourceid1', $row->entityform_id);
        $res = $query->execute()->fetchAssoc();
        if ($res !== FALSE) {
          // Manually push in the value entity ID.
          $row->migrate_map_destid1 = $res['destid1'];
        }
      }
    }
  }

  /**
   * Child class conditions.
   */
  protected function customConditions() {}

  /**
   * Child class conditions.
   */
  protected function customCountConditions() {}

  /**
   * Set entity form type ID.
   *
   * @param string $id
   *   The entity form ID.
   */
  protected function setBundleId($id) {
    $this->entityFormMachineName = $id;
  }

  /**
   * Add a field wrapper.
   *
   * @param FieldBaseWrapper $wrapper
   */
  public function addFieldWrapper(&$wrapper) {
    $wrapper->setMigration($this);
    $wrapper->setBaseTableName('field_revision_');
    $this->fieldWrappers[] = $wrapper;
  }

  /**
   *
   * @param type $wrappers
   */
  public function addFieldWrappers($wrappers) {
    // Update migration instance.
    /** @var FieldBaseWrapper $fieldWrapper */
    foreach ($wrappers as $fieldWrapper) {
      $this->addFieldWrapper($fieldWrapper);
    }
  }

  /**
   * The entity form ID.
   *
   * @return string
   *   The entity form ID.
   */
  public function getBundleId() {
    return $this->entityFormMachineName;
  }

  public function getFieldWrappers() {
    return $this->fieldWrappers;
  }

  public function getFieldJoinCondition($fieldName, $tableAlias, $wrapperClass, $query) {
    return $tableAlias . ".revision_id = r.vid AND " . $tableAlias . ".entity_type = 'entityform'";
  }

  public function complete($entityForm, $row) {
    // Fix uid and timestamp in node_revisions.
    $query = db_update('entityform_revision')
      ->condition('vid', $entityForm->vid);
    $fields['changed'] = $entityForm->changed;
    $fields['uid'] = $entityForm->uid;
    $query->fields($fields);
    $query->execute();
  }

}
