<?php

/**
 * Wrapper for import entity reference fields on migrate.
 */
class FieldEntityReferenceWrapper extends FieldBaseWrapper {

  /**
   * {@inheritDoc}
   */
  public function __construct($fieldName, $multivalue = FALSE) {
    parent::__construct($fieldName, $multivalue);

    $this->field_type = 'entityreference';
  }

  /**
   * {@inheritDoc}
   */
  public function addFields($query) {
    if (!$this->isMultivalue()) {
      $query->fields($this->tableAlias, [$this->fieldName . '_target_id']);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function addFieldMapping() {
    if (empty($this->source_migration)) {
      $this->migration->addFieldMapping($this->fieldName, $this->fieldName . '_target_id');
    }
    else {
      $this->migration->addFieldMapping($this->fieldName, $this->fieldName . '_target_id')
        ->sourceMigration($this->source_migration);
    }
  }

  /**
   * {@inheritDoc}
   */
  protected function buildDatabaseValueColumn() {
    $this->databaseValueColumn = $this->fieldName . '_target_id';
  }

  /**
   * Save field data in her table.
   *
   * @param DatabaseConnection $connection
   *   Database connecto.
   * @param object $entity
   *   Parent entity.
   * @param string $language
   *   Language code.
   * @param string|array $sourceData
   *   Source field data.
   * @param string $bundle
   *   Host gield name.
   *
   * @throws Exception
   */
  public function saveFieldData($connection, $entity, $language, $sourceData, $bundle) {
    $query = $connection->insert($this->getBaseTableName());
    if (!$sourceData) {
      $sourceData = 0;
      //watchdog(WATCHDOG_WARNING, "{$bundle} / {$this->fieldName}_target_id = NULL in " . get_class($this));
    }
    if (is_array($sourceData)) {
      $sourceData = $sourceData['destid1'];
    }
    $query->fields([
      'entity_type' => $entity->entityType(),
      'bundle' => $bundle,
      'deleted' => 0,
      'entity_id' => $entity->item_id,
      'revision_id' => $entity->revision_id,
      'language' => $language,
      'delta' => 0,
      $this->fieldName . '_target_id' => $sourceData,
    ]);
    $query->execute();
  }

}
