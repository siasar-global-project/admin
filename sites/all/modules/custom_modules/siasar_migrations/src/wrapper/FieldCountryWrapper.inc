<?php

/**
 * Wrapper for import country fields on migrate.
 */
class FieldCountryWrapper extends FieldBaseWrapper {

  /**
   * {@inheritDoc}
   */
  public function __construct($fieldName, $multivalue = FALSE) {
    parent::__construct($fieldName, $multivalue);
    $this->field_type = 'country';
  }

  /**
   * Prepare tables.
   *
   * @param SelectQueryInterface $query
   */
  public function addTables($query) {
    if (!$this->isMultivalue()) {
      $query->join(
        $this->baseTableName . $this->fieldName,
        $this->tableAlias,
        $this->getJoinCondition($query)
      );
    }
  }

  /**
   * Prepare fields.
   *
   * @param SelectQueryInterface $query
   */
  public function addFields($query) {
    if (!$this->isMultivalue()) {
      $query->fields($this->tableAlias, [$this->fieldName . '_iso2']);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function addFieldMapping() {
    $this->migration->addFieldMapping($this->fieldName, $this->fieldName . '_iso2');
  }

  /**
   * {@inheritDoc}
   */
  protected function buildDatabaseValueColumn() {
    $this->databaseValueColumn = $this->fieldName . '_iso2';
  }

}
