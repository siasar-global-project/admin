<?php

/**
 * Wrapper for import geofield fields on migrate.
 *
 * This field has no compatibility with multi-value process.
 */
class FieldGeofieldWrapper extends FieldBaseWrapper {

  /**
   * {@inheritDoc}
   */
  public function __construct($fieldName) {
    parent::__construct($fieldName);

    $this->field_type = 'geofield';
  }

  /**
   * {@inheritDoc}
   */
  public function addFields($query) {
    $query->fields($this->tableAlias, [
      "{$this->fieldName}_lat",
      "{$this->fieldName}_lon",
      "{$this->fieldName}_geom",
      "{$this->fieldName}_geo_type",
      "{$this->fieldName}_left",
      "{$this->fieldName}_top",
      "{$this->fieldName}_right",
      "{$this->fieldName}_bottom",
      "{$this->fieldName}_geohash",
    ]);
  }

  /**
   * Prepare fields.
   *
   * @param array $subfields
   *   Key is the field name, value is the field description.
   */
  public function addSourceSubFields(&$subfields) {
    $subfields[$this->fieldName . '_geo_type'] = $this->tableAlias . '.' . $this->fieldName . '_geo_type';
    $subfields[$this->fieldName . '_left'] = $this->tableAlias . '.' . $this->fieldName . '_left';
    $subfields[$this->fieldName . '_top'] = $this->tableAlias . '.' . $this->fieldName . '_top';
    $subfields[$this->fieldName . '_right'] = $this->tableAlias . '.' . $this->fieldName . '_right';
    $subfields[$this->fieldName . '_bottom'] = $this->tableAlias . '.' . $this->fieldName . '_bottom';
    $subfields[$this->fieldName . '_geohash'] = $this->tableAlias . '.' . $this->fieldName . '_geohash';
    $subfields[$this->fieldName . '_geom'] = $this->tableAlias . '.' . $this->fieldName . '_geom';
  }

  /**
   * Prepare fields.
   *
   * @param array $subfields
   *   Value is the field name.
   */
  public function addUnmigratedDestinationSubFields(&$subfields) {
    $subfields[] = $this->fieldName . ':wkt';
    $subfields[] = $this->fieldName . ':json';
  }

  /**
   * Prepare fields.
   *
   * @param array $subfields
   *   Key is the field name, value is the field description.
   */
  public function addDestinationSubFields(&$subfields) {
    $subfields[$this->fieldName . ':geohash'] = 'geohash';
    $subfields[$this->fieldName . ':geom'] = 'geom';
  }

  /**
   * {@inheritDoc}
   */
  public function addFieldMapping() {
    $this->migration->addFieldMapping($this->fieldName)->defaultValue('Point');
    $this->migration->addFieldMapping("{$this->fieldName}:input_format")->defaultValue('lat/lon');

    $this->migration->addFieldMapping("{$this->fieldName}:lat", "{$this->fieldName}_lat");
    $this->migration->addFieldMapping("{$this->fieldName}:lon", "{$this->fieldName}_lon");
    $this->migration->addFieldMapping("{$this->fieldName}:geom", "{$this->fieldName}_geom");
    $this->migration->addFieldMapping("{$this->fieldName}:geo_type", "{$this->fieldName}_geo_type")->defaultValue('point');
    $this->migration->addFieldMapping("{$this->fieldName}:left", "{$this->fieldName}_left");
    $this->migration->addFieldMapping("{$this->fieldName}:top", "{$this->fieldName}_top");
    $this->migration->addFieldMapping("{$this->fieldName}:right", "{$this->fieldName}_right");
    $this->migration->addFieldMapping("{$this->fieldName}:bottom", "{$this->fieldName}_bottom");
    $this->migration->addFieldMapping("{$this->fieldName}:geohash", "{$this->fieldName}_geohash");
  }

  /**
   * Save field data in her table.
   *
   * @param DatabaseConnection $connection
   *   Database connecto.
   * @param object $entity
   *   Parent entity.
   * @param string $language
   *   Language code.
   * @param array $sourceData
   *   Source field data.
   * @param string $bundle
   *   Host gield name.
   *
   * @throws Exception
   */
  public function saveFieldData($connection, $entity, $language, $sourceData, $bundle) {
    $query = $connection->insert($this->getBaseTableName());
    $query->fields([
      'entity_type' => $entity->entityType(),
      'bundle' => $bundle,
      'deleted' => 0,
      'entity_id' => $entity->item_id,
      'revision_id' => $entity->revision_id,
      'language' => $language,
      'delta' => 0,
      $this->fieldName . '_bottom' => $sourceData['arguments']['bottom'],
      $this->fieldName . '_geo_type' => $sourceData['arguments']['geo_type'],
      $this->fieldName . '_geohash' => $sourceData['arguments']['geohash'],
      $this->fieldName . '_geom' => $sourceData['arguments']['geom'],
      $this->fieldName . '_lat' => $sourceData['arguments']['lat'],
      $this->fieldName . '_left' => $sourceData['arguments']['left'],
      $this->fieldName . '_lon' => $sourceData['arguments']['lon'],
      $this->fieldName . '_right' => $sourceData['arguments']['right'],
      $this->fieldName . '_top' => $sourceData['arguments']['top'],
    ]);
    $query->execute();
  }

}
