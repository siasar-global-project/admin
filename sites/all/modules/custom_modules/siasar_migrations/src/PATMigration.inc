<?php

/**
 * Migration for PAT entityforms.
 */
class PATMigration extends SiasarMigrationBase {
  public function __construct($arguments = []) {
    // Set entity form type ID.
    $this->setBundleId('prestador_de_asistencia_t_cnica');

    // Create field maps.
    $this->addFieldWrappers([
      // Country.
      new FieldCountryWrapper('field_pais'),
      // Workflow.
      new FieldWorkflowWrapper('field_status'),

      // Date fields.
      new FieldDateWrapper('field_fecha_de_aplicacion'),

      // Text fields.
      new FieldTextWrapper('field_entity_name'),
      new FieldTextWrapper('field_otras_tipo_apoyo'),
      new FieldTextWrapper('field_data_source'),

      new FieldLongTextWrapper('field_observaciones_comentarios'),

      // Number fields.
      new FieldDecimalWrapper('field_altitud'),
      new FieldDecimalWrapper('field_cantidad_equipos_transport'),
      new FieldDecimalWrapper('field_cantidad_medicion_agua'),
      new FieldDecimalWrapper('field_cantidad_equipos_inform'),

      new FieldIntegerWrapper('field_total_comunidades_zona'),
      new FieldIntegerWrapper('field_cantidad_comunidades_apoya'),
      new FieldIntegerWrapper('field_cantidad_tecnicos_zona'),
      new FieldIntegerWrapper('field_verificar_apoyar_caps'),
      new FieldIntegerWrapper('field_apoyar_juridica_caps'),
      new FieldIntegerWrapper('field_revisar_actualizar_finanza'),
      new FieldIntegerWrapper('field_apoyar_actualizar_tarifa'),
      new FieldIntegerWrapper('field_apoyar_legalizacion_reglam'),
      new FieldIntegerWrapper('field_apoyar_caps_planificacion'),
      new FieldIntegerWrapper('field_apoyar_caps_conflictos'),
      new FieldIntegerWrapper('field_recolectar_muestras_agua'),
      new FieldIntegerWrapper('field_caps_medicion_pozo_agua'),
      new FieldIntegerWrapper('field_actualizar_prov_servicio'),
      new FieldIntegerWrapper('field_asesorar_prestador_aom'),
      new FieldIntegerWrapper('field_siasar_planification_anual'),
      new FieldIntegerWrapper('field_otras_numero_comunidade'),
      new FieldIntegerWrapper('field_closed_revisions', TRUE),

      // Boolean fields.
      new FieldBooleanWrapper('field_cuenta_presupuesto_anual'),
      new FieldBooleanWrapper('field_dispone_fondos_combistible'),
      new FieldBooleanWrapper('field_dispone_servicios_internet'),
      new FieldBooleanWrapper('field_dispone_material_didactico'),
      new FieldBooleanWrapper('field_dispone_fondos_viaticos'),

      // Image fields
      new FieldImageReferenceWrapper('field_imagenpat'),

      // Reference fields.
      new FieldEntityReferenceWrapper('field_user_reference'),
      new FieldEntityReferenceWrapper('field_rating_s1'),

      new FieldTermReferenceWrapper('field_tipo_de_pat2'),
      new FieldTermReferenceWrapper('field_estado_equipos_transporte'),
      new FieldTermReferenceWrapper('field_estado_medicion_agua'),
      new FieldTermReferenceWrapper('field_estado_equipos_informatico'),
      new FieldTermReferenceWrapper('field_estado_fondos_viaticos'),
      new FieldTermReferenceWrapper('field_estado_fondos_combustible'),
      new FieldTermReferenceWrapper('field_estado_servicios_internet'),
      new FieldTermReferenceWrapper('field_estado_material_didactico'),

      // Measure fields.
      new FieldMeasureWrapper('field_monto'),

      // Geofield.
      new FieldGeofieldWrapper('field_latitud'),
    ]);

    $local_entity_field = new FieldTermReferenceWrapper('field_entidad_local');
    $local_entity_field->setSubmigration('tid', 'AdministrativeDivision');

    $this->addFieldWrapper($local_entity_field);

    // Prepare migration.
    $this->description = "Migration of '{$this->entityFormMachineName}' Entityform submissionss from SIASAR database";
    $this->dependencies = ['AdministrativeDivision', 'Users'];
    parent::__construct($arguments);
  }

  /**
   * Child class conditions.
   */
  protected function customConditions() {
    // Custom conditions.
    $this->query->condition(FieldBaseWrapper::getDefaultTableAlias('field_pais') . '.' . 'field_pais_iso2', 'KG');
  }

  /**
   * Child class conditions.
   */
  protected function customCountConditions() {
    $countryField = new FieldCountryWrapper('field_pais');
    $countryField->setMigration($this);
    $countryField->addTables($this->count_query);
    // Custom conditions.
    $this->count_query->condition(FieldBaseWrapper::getDefaultTableAlias('field_pais') . '.' . 'field_pais_iso2', 'KG');
  }

}
