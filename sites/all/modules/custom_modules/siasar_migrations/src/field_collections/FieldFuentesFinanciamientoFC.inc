<?php

/**
 * Migration class for field_a_fuentes_de_financiamient field collection.
 */
class FieldFuentesFinanciamientoFC extends SiasarFieldCollectionMigrationBase {
  public function __construct($arguments = []) {
    // Set field collection name.
    $this->setFieldCollectionName('field_a_fuentes_de_financiamient');
    // Set country to filter.
    $this->setCountry('KG');
    // Set source migration to get host entity.
    $this->sourceMigration = 'System';

    // Reference fields.
    $this->addFieldWrapper(new FieldEntityReferenceWrapper('field_origen_de_financiamiento_i'));
    $this->addFieldWrapper(new FieldEntityReferenceWrapper('field_programa_especifico_fondos'));

    // Measured decimal fields.
    $this->addFieldWrapper(new FieldMeasureWrapper('field_monto_financiamiento'));

    parent::__construct($arguments);
  }

  /**
   * {@inheritDoc}
   */
  protected function setDependencies() {
    $this->dependencies = ['System'];
  }

}
