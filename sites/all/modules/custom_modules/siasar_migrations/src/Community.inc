<?php

/**
 * Migration for PAT entityforms.
 */
class Community extends SiasarMigrationBase {
  public function __construct($arguments = []) {
    // Set entity form type ID.
    $this->setBundleId('comunidad');

    // Create field maps.
    $this->addFieldWrappers([
      new FieldCountryWrapper('field_pais'),
      new FieldDateWrapper('field_fecha_encuesta'),
      new FieldEntityReferenceWrapper('field_user_reference'),
      new FieldImageReferenceWrapper('field_imagenc'),
      new FieldTextWrapper('field_entity_name'),
      new FieldGeofieldWrapper('field_com_georef'),
      new FieldDecimalWrapper('field_com_altitud'),
      new FieldTextWrapper('field_com_codigo_comunidad'),
      new FieldEntityReferenceWrapper('field_cuenca_hidrografica'),
      new FieldEntityReferenceWrapper('field_area_planificacion'),
      new FieldEntityReferenceWrapper('field_otras_divisiones'),
      new FieldIntegerWrapper('field_com_poblacion_total'),
      new FieldEntityReferenceWrapper('field_etnia_en_mayoria'),
      new FieldEntityReferenceWrapper('field_idioma_predominante'),
      new FieldLongTextWrapper('field_field_com_obs_poblacion'),
      new FieldIntegerWrapper('field_com_viv_totales'),
      new FieldIntegerWrapper('field_com_viv_sin_sistema'),
      new FieldBooleanWrapper('field_com_srv_existe'),
      new FieldBooleanWrapper('field_com_srv_telf_existe'),
      new FieldBooleanWrapper('field_com_srv_telm_existe'),
      new FieldBooleanWrapper('field_com_srv_inte_existe'),
      new FieldLongTextWrapper('field_com_srv_caracteristicas'),
      new FieldEntityReferenceWrapper('field_data_collection_method', TRUE),
      new FieldIntegerWrapper('field_com_num_viv_muestra'),
      new FieldIntegerWrapper('field_com_num_viv_infta'),
      new FieldIntegerWrapper('field_com_num_viv_inftb'),
      new FieldIntegerWrapper('field_field_com_num_viv_infnt'),
      new FieldIntegerWrapper('field_com_num_viv_usoinfa'),
      new FieldIntegerWrapper('field_com_num_viv_usohab'),
      new FieldIntegerWrapper('field_com_num_viv_usoinfb'),
      new FieldIntegerWrapper('field_com_num_viv_usotodos'),
      new FieldIntegerWrapper('field_com_num_viv_usoinfa2'),
      new FieldIntegerWrapper('field_com_num_viv_usohab2'),
      new FieldIntegerWrapper('field_com_num_viv_usoinfb2'),
      new FieldIntegerWrapper('field_com_num_viv_usotodos2'),
      new FieldIntegerWrapper('field_com_no_uso_vivnumuso'),
      new FieldIntegerWrapper('field_com_no_uso_vivnumtodos'),
      new FieldIntegerWrapper('field_com_hig_num_vivbasica'),
      new FieldIntegerWrapper('field_com_hig_num_vivtodos'),
      new FieldIntegerWrapper('field_com_hig_num_vivagua'),
      new FieldBooleanWrapper('field_com_recotrata_prac'),
      new FieldIntegerWrapper('field_com_recontrata_numvivc'),
      new FieldTextListWrapper('field_com_recotrata_disp'),
      new FieldBooleanWrapper('field_com_ce_existe'),
      new FieldTextWrapper('field_com_ce_nombre'),
      new FieldTermReferenceWrapper('field_ce_location'),
      new FieldBooleanWrapper('field_com_cs_existe'),
      new FieldTextWrapper('field_com_cs_nombre'),
      new FieldTermReferenceWrapper('field_cs_location'),
      new FieldLongTextWrapper('field_com_obs_obscomre'),
      new FieldEntityReferenceWrapper('field_rating_s1'),
      new FieldTextWrapper('field_data_source'),
      new FieldWorkflowWrapper('field_status'),
      new FieldIntegerWrapper('field_closed_revisions', TRUE),
    ]);

    $local_entity_field = new FieldTermReferenceWrapper('field_entidad_local');
    $local_entity_field->setSubmigration('tid', 'AdministrativeDivision');

    $this->addFieldWrapper($local_entity_field);

    // Set dependencies.
    $this->dependencies = ['Files', 'AdministrativeDivision', 'Users'];

    // Prepare migration.
    parent::__construct($arguments);
  }

  /**
   * Child class conditions.
   */
  protected function customConditions() {
    // Custom conditions.
    $this->query->condition(FieldBaseWrapper::getDefaultTableAlias('field_pais') . '.' . 'field_pais_iso2', 'KG');
  }

  /**
   * Child class conditions.
   */
  protected function customCountConditions() {
    $countryField = new FieldCountryWrapper('field_pais');
    $countryField->setMigration($this);
    $countryField->addTables($this->count_query);
    // Custom conditions.
    $this->count_query->condition(FieldBaseWrapper::getDefaultTableAlias('field_pais') . '.' . 'field_pais_iso2', 'KG');
  }

}
