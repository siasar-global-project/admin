<?php

/**.
 * Drush command declaration function.
 * @return mixed
 */
function siasar_entityform_drush_command() {

  $items['siasar-forms-process-all'] = array(
    'description' => 'Process all the entityforms in the process queue.',
    'aliases'     => array('sfpa'),
    'callback'    => 'siasar_forms_process_all',
  );

  $items['siasar-forms-process-items'] = array(
    'description' => 'Process a certain number of entityforms in the process queue.',
    'aliases'     => array('sfpi'),
    'callback'    => 'siasar_forms_process_items',
    'arguments'   => array(
      'items' => "Number of items to process on each query. Default value: 30 items",
    ),
  );

  $items['siasar-forms-process-pending'] = array(
    'description' => 'Get the amount of elements pending.',
    'aliases'     => array('sfpp'),
    'callback'    => 'siasar_forms_process_pending',
  );

  $items['siasar-forms-process-survey'] = array(
    'description' => 'Process a given survey.',
    'aliases'     => array('sfps'),
    'callback'    => 'siasar_forms_process_validate_survey',
    'arguments'   => array(
      'item' => "Survey ID",
    ),
  );

  $items['siasar-forms-process-country'] = array(
    'description' => 'Process all forms from a given country.',
    'aliases' => array('sfpc'),
    'callback' => 'siasar_forms_process_country_forms',
    'arguments' => array(
      'country' => 'Country ISO2 code',
    ),
    'options' => array(
      'limit' => 'Limit the process to a number of items.',
    ),
    'examples' => array(
      'drush sfpc KG' => 'Process all KG forms.',
      'drush sfpc KG --limit=100' => 'Process 100 items of KG country forms.'
    ),
  );

  $items['siasar-forms-reset-states'] = array(
    'description' => 'Reset the states of all the surveys and queue them for processing.',
    'aliases'     => array('sfrs'),
    'callback'    => 'siasar_forms_reset_states',
  );

  $items['siasar-forms-last-execution-time'] = array(
    'description' => 'Get the last time execution of elements in the queue.',
    'aliases'     => array('sflet'),
    'callback'    => 'siasar_forms_get_last_execution_time',
  );

  $items['siasar-forms-requeue-elements'] = array(
    'description' => 'Re-insert in the queue all the elementes with a null revision.',
    'aliases'     => array('sfre'),
    'callback'    => 'siasar_forms_requeue_elements',
  );


  return $items;
}

/**
 * Process certain number of forms.
 *
 * @param $items integer Amount of item to process.
 */
function siasar_forms_process_items($items) {

  if ($items) {
    $elemnts = siasar_process_pending_computable_entityforms($items);
    drush_print('Processed ' . $elemnts . ' elements');
  }

}

/**
 * Process all the pending forms in the system.
 */
function siasar_forms_process_all() {

  while (siasar_process_pending_computable_entityforms() > 0) {
  }

}

/**
 * Get the number of pending forms in the system.
 */
function siasar_forms_process_pending() {
  $result = db_query("select count(*) from  entityform_computable_processing");

  $items = reset($result->fetchAll());

  drush_print('Pending elements: ' . $items->count);
}

/**
 * Get if an specified survey has a computable version
 * @param $id int The entityform ID.
 *
 * @return string
 */
function siasar_forms_process_validate_survey($id) {
  if ($id) {
    $computable = _computable_revision($id);
    if ($computable) {
      return 'The valid revision for ' . $id . ' is ' . $computable;
    }
  }
  drush_print('No computable revision');
}

/**
 * Get the last execution time of elements in the queue.
 */
function siasar_forms_get_last_execution_time() {
  drush_print(variable_get('siasar_form_last_execution_time', 'No elements has been processed yet.'));
}

/**
 * Reset states and the processing queue.
 */
function siasar_forms_reset_states() {
  //Completely reset the states and the processing queue.
  reset_entityform_states();
}

/**
 * Insert again in the queue all the elements without revision
 */
function siasar_forms_requeue_elements(){
  $entityforms = db_select('entityform_computable_states', 'efcs')
    ->fields('efcs', ['entityform_id'])
    ->isNull('entityform_revision')
    ->execute();

  foreach ($entityforms->fetchAll() as $survey){
    _update_processing_queue_element($survey->entityform_id);
  }
}

/**
 * Callback for siasar-forms-process-country command.
 * Process all forms for a country.
 *
 * @param $country
 *   The country ISO2 code.
 */
function siasar_forms_process_country_forms($country) {
  if (!empty($country)) {
    // Check if the given code is a valid country code.
    if (country_load($country)) {
      // Get limit option.
      $limit = drush_get_option('limit', 0);

      // Get all forms for the country.
      $query = db_select('entityform', 'eform');
      $query->innerJoin('field_data_field_pais', 'country', "country.entity_id = eform.entityform_id AND country.entity_type = 'entityform'");
      $query->addField('eform', 'entityform_id');
      $query->condition('country.field_pais_iso2', $country);

      // Transform to integer.
      $limit = (int)$limit;

      // Apply limit if the option is given.
      if (is_integer($limit) && $limit !== 0) {
        $query->range(0, $limit);
      }

      $results = $query->execute()->fetchCol();

      // Process all results.
      if (!empty($results)) {
        foreach ($results as $key => $result) {
          _computable_revision($result);

          if ($key !== 0 && $key % 100 == 0) {
            drush_print("{$key} items processed.");
          }
        }

        if (!empty($key)) {
          $key = $key + 1;
          drush_print("Total items processed: {$key}");
        }
      }
      else {
        drush_print("No results found for {$country}.");
      }
    }
    else {
      drush_print('Country not found or is not a valid code.');
      return;
    }
  }
}
