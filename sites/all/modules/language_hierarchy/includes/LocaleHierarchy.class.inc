<?php
/**
 * @file
 * Definition of LocaleHierarchy.
 *
 * Inspired by Language fallback module.
 */

/**
 * Class LocaleHierarchy.
 */
class LocaleHierarchy implements ArrayAccess {
  private $langcode;
  private $cache = array();
  private $candidates = NULL;

  public function __construct($langcode) {
    $this->langcode = $langcode;
    $this->cache = array();
  }

  public function offsetExists($offset) {
    /**
     * see https://bugs.php.net/bug.php?id=69659
     *
     * Before PHP 7.0.6, "Calling isset() or empty() on a dimension of an object that implements ArrayAccess results in
     * the offsetExists method being called. When calling isset() or empty() on a subdimension of a dimension of that
     * object, only the offsetGet method is called, not the offsetExists method. This is inconsistent, undocumented, and
     * often results in undefined index notices being raised."
     *
     * With PHP 7.0.6+, "Instead, when isset() or empty() is called on a subdimension of an object implementing
     * ArrayAccess, the offsetExists method" is "called to check if the dimension exists. If the dimension
     * does not exist, then offsetGet" is not "called."
     *
     * In other words, with PHP 7.0.6+ $cache[$offset] is never set by calls coming from t() unless we force it here.
     */
    if (!isset($this->cache[$offset])) {
        $this->offsetBuild($offset);
    }

    return isset($this->cache[$offset]);
  }

  public function offsetGet($offset) {
    $this->offsetBuild($offset);

    return $this->cache[$offset];
  }

  private function offsetBuild($offset) {
    /**
     * WARNING!
     * On some rare occasions when drupal is not fully bootstrapped
     * the function language_hierarchy_get_ancestors might not be available.
     * This may happen on some administration pages so it should be
     * safe to just skip all defined fallbacks.
     */
    if ($this->candidates === NULL && function_exists('language_hierarchy_get_ancestors')) {
      drupal_static_reset('language_list');
      $this->candidates = array_keys(language_hierarchy_get_ancestors($this->langcode));
    }

    if (!isset($this->cache[$offset])) {
      $this->cache[$offset] = new LocaleContextHierarchy($this->langcode, $this->candidates, $offset);
    }
  }

  public function offsetSet($offset, $value) {return;}
  public function offsetUnset($offset) {return;}
}
